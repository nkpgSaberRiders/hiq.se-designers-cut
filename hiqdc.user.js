// ==UserScript==
// @name HiQ Designer's Cut
// @version 0.1.7
// @description Various visual improvements and toying around with the design of HiQ.se.
// @namespace http://hiq.se/hiqdc
// @match *://hiq.se/*
// @match *://hiq.fi/*
// @match *://www.hiq.se/*
// @match *://www.hiq.fi/*
// ==/UserScript==

// Load Stuff
// 
// 

$('head').append('<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,500,700i,900,900" rel="stylesheet">');

// Utility
// 
// 
$('head').append('<style id="fixes"></style>');
$('#fixes').append('.news a { transition: background-color .25s ease-out,color .25s ease-out }');

// Scroll entice
$('.hide-for-small-only').append('<div class="scroll-msg-container"><div class="scroll-msg-inner"><div class="scroll-msg-wheel"></div></div></div>');
$('#fixes').append('.hide-for-small-only{position:relative}@-webkit-keyframes mouse-scroll{0%{top:10%;opacity:1}20%{opacity:.6}100%,33%{top:60%;opacity:0}}@-moz-keyframes mouse-scroll{0%{top:10%;opacity:1}20%{opacity:.6}100%,33%{top:60%;opacity:0}}@-o-keyframes mouse-scroll{0%{top:10%;opacity:1}20%{opacity:.6}100%,33%{top:60%;opacity:0}}@keyframes mouse-scroll{0%{top:10%;opacity:1}20%{opacity:.6}100%,33%{top:60%;opacity:0}}.scroll-msg-container{position:absolute;top:90%;left:50%;width:28px;height:54px;margin:-27px 0 0 14px}.scroll-msg-inner{width:24px;height:50px;position:relative;border-radius:16px;border:2px solid #fff}.scroll-msg-wheel{position:absolute;top:10%;left:50%;width:6px;height:6px;margin-left:-3px;border-radius:50%;background-color:#fff;-webkit-animation:mouse-scroll 3s infinite;-moz-animation:mouse-scroll 3s infinite;-o-animation:mouse-scroll 3s infinite;animation:mouse-scroll 3s infinite}');

$(window).scroll(function(){
  $(".scroll-msg-container").css("opacity", 1 - $(window).scrollTop() / $(window).height()*2);
});


// Menu fixes 
// 
// 
$('#fixes').append('.top-bar-wrapper .top-bar { top: -30px; }');
$('#fixes').append('@media screen and (min-width: 64em) { .header-right { padding: 26px 60px 0; } }');
$('#fixes').append('\
.overlaylang-slidedown, .overlaymenu-slidedown, .overlaysearch-slidedown {\
    opacity: 0;\
    visibility: hidden;\
    -webkit-transform: translateY(-100%);\
    transform: translateY(-100%);\
    -webkit-transition: -webkit-transform .3s ease-in-out;\
    transition: all .3s ease-in-out;\
}');
$('#fixes').append('.overlaylang-slidedown.open, .overlaymenu-slidedown.open, .overlaysearch-slidedown.open { opacity: 1; transition: all .3s ease-in-out; }');
$('#fixes').append('#trigger-langoverlay { margin-top: 5px;}');
$('#fixes').append('.overlaymenu-slidedown, .overlaysearch-slidedown, .overlaylang-slidedown {top: 30px;}');
$('#fixes').append('.menu-arrow {position:relative; top: -4px;}');

// Titelblock
// 
// 
$('.hero-text').addClass('medium-11').addClass('large-9');
$('#fixes').append('@media only screen and (min-width: 64.063em) { .hero-75 .font-size-64 {font-size: 92px; line-height: 0.875em;}}');


// Margins
// 
// 

$('#fixes').append('#load-more-container {margin-top: 40px;}');

// Vad vi gör, Om oss, Kontakta oss, Case,  
$('.body-container').find('.content-phrase-block .row').first().css('padding-top', '40px');

// Vad vi gör
$('.case-listing').parent().css('padding', '40px 0');
$('div.bg-gray-light').next().find('.content-phrase-block').css('padding', '40px 0');

// Pink Stuff
// 
// 
$('#fixes').append('.row.bg-pink, .case-page-contact-area { background: #fff url(https://bytebucket.org/nkpgSaberRiders/hiq.se-designers-cut/raw/master/img/PNG_Textures_PinkOnly16cOpt.png) no-repeat 50%!important; background-size: cover !important; }');
$('.quotation').css('position', 'relative');
$('#fixes').append('.quotation>div:before:not(.quotation-cite) {content: "\\0022"; font-family: HiQRough, sans-serif; float: left; position: absolute; top: 40px; left: 20px; color: #ffe50d; font-size: 100px;}');
$('#fixes').append('.quotation .phrase-white { margin: -40px 0px 20px 40px; }');
$('#fixes').append('.quotation { padding: 70px 50px}');
$('#fixes').append('.vcard-info {margin-top: 16px;}');
$('#fixes').append('.row.bg-pink { min-height: 400px;}');

// Rekrytblock
$('#fixes').append('.talk .talk-phrase { color: #ffe50d; font-size: 50px}');
$('#fixes').append('.recruit-holder { display: table; height: 250px; } .recruit-button { display: table-cell; vertical-align: middle; }');
$('#fixes').append('.recruit-holder a { font-size: 75px; line-height: 0.875em; }');
$('#fixes').append('.talk a { padding: 30px; }');
$('#fixes').append('.talk .talk-phrase { margin-top: -5px; padding-left: 30px; }');
$('.bg-pink .talk').addClass('recruit-button').parent().addClass('recruit-holder');
$('.bg-pink .talk').removeClass('large-4').addClass('large-8').removeClass('medium-6').addClass('medium-10').removeClass('small-8').addClass('small-10');
$('#fixes').append('.bg-pink .talk:after { float: right; width: 90px; position: relative; right: 45px; bottom: 15px; content: url(https://www.hiq.se/Static/image/arrow-right-yellow.png); }');
$('#fixes').append('.recruit-button a:hover { color: #ffe50d}');



// Font fixes
// 
// 
$('#fixes').append('body { font-family: Roboto, Helvetica, Arial, sans-serif;}');
$('#fixes').append('.orbit { font-family: Roboto; font-weight: 300; font-style: italic;}');
$('#fixes').append('.font-light { font-weight: 300;} .font-medium { font-weight: 500;} .font-bold { font-weight: 700;} .font-black { font-weight: 900;}');
$('.news .button').removeClass('font-roboto-bold').addClass('font-black');
$('#fixes').append('.quotation a { color: #fff; }');
$('#fixes').append('article header time { font-size: 24px;}');
$('#fixes').append('article header time .day { font-family: Roboto, sans-serif; font-weight: 900;}');
$('#fixes').append('article header time .month { font-family: Roboto, sans-serif; font-weight: 900;}');
$('#fixes').append('article header time .year { font-family: Roboto, sans-serif; font-weight: 300;}');
$('#fixes').append('article header time .category { font-family: Roboto, sans-serif; font-weight: 300;}');
$('#fixes').append('article header time .category-separator { font-family: Roboto, sans-serif; font-weight: 300;}');
$('#fixes').append('.font-roboto-bold { font-family: Roboto, sans-serif; font-weight: 900;}');
$('#fixes').append('.font-roboto-bold, h1, h2, h3, h4, h5, h6 { font-family: Roboto, sans-serif; font-weight: 900;}');
$('#fixes').append('.font-hiq, .font-hiq-rough { font-weight: 400 !important; line-height: 0.9em !important}');
$('#fixes').append('.fn {font-weight: 400}');
$('#fixes').append('.hero h1, .hero-75 h1, .hero-50 h1, .hero-25 h1 {position: relative; left: -9px; padding-left: 9px; text-transform:none; animation: 1s ease-out 0s 1 fadeIn;}');
$('#fixes').append('.font-hiq-rough { font-family: HiQ,HiQRough,sans-serif; -webkit-mask-image: url("https://bytebucket.org/nkpgSaberRiders/hiq.se-designers-cut/raw/master/img/PNG_Textures_PinkOnly16cOpt.png"); mask-image: url("https://bytebucket.org/nkpgSaberRiders/hiq.se-designers-cut/raw/master/img/PNG_Textures_PinkOnly16cOpt.png")};');
$('#fixes').append('@keyframes slideInFromLeft {0% {transform: opacity(0);} 100% {transform: opacity(1);}}');
$('#fixes').append('.yellow-text-pink-shadow::first-line {display: block;color: #ffe50d;text-shadow: -9px 0 0 #fe0096;margin: 0;}.yellow-text-pink-shadow {display: block;color: #fe0096;margin: 0;text-shadow:none}');
$('#fixes').append('.pink-text-yellow-shadow::first-line {display: block;color: #fe0096;text-shadow: -9px 0 0 #ffe50d;margin: 0;}.pink-text-yellow-shadow {display: block;color: #ffe50d;margin: 0;text-shadow:none}');



// Careers
// 
$('#fixes').append('.apply a {padding: 22px;} .apply span {position: relative !important; top: 10px;}');
$('#fixes').append('.career-grid-list:after { content: ""; display: block; padding-bottom: 100%;}');
$('#fixes').append('.career-grid-list-info { position: absolute; bottom: 0px; width: 100%;}');
$('#fixes').append('.people h1 {font-size: 92px;} .people .phrase { margin-top: -15px;}');
$('#fixes').append('.Form__MainBody>section.Form__Element>.Form__Element textarea {line-height: 30px;}');
$('li').wrapInner('<span class="licontent"></span>');
$('#fixes').append('.jobpost li {margin-top: 32px;} ol, ul {margin-left: 1.5rem;} .jobpost ul li:before {position: relative; top: -5px; margin: 0px 16px 0px 0px; width: 12px; font-size: 21px; display: inline-block; vertical-align: top;} .licontent {display: inline-block; vertical-align: top; width: 80%;}');
$('#fixes').append('.jobpost-content .vcard-info a { color: #000;}');

// Location
// 
// 
$('#fixes').append('#contact-info-container .vcard .font-hiq {margin-bottom: 28px;}');
$('#fixes').append('#contact-info-container .vcard .adr {line-height: 1.3em; font-weight: 500;}');
$('#fixes').append('@media screen and (min-width: 64em) { #jobPost-container h1.color-pink { font-size: 90px; } }');


// Ideas
$('#fixes').append('.ideas-header-line-height { font-family: Roboto,sans-serif; font-weight: 900; line-height: 54px; }');

$('#fixes').append('article header.idea-bg-pink a { background-image: linear-gradient(0deg,transparent,transparent 50%,#fe0096 0,#fe0096 100%,transparent 100%); }');
$('#fixes').append('article header.idea-bg-green a { background-image: linear-gradient(0deg,transparent,transparent 50%,#00c493 0,#00c493 100%,transparent 100%); }');
$('#fixes').append('article header.idea-bg-yellow a { background-image: linear-gradient(0deg,transparent,transparent 50%,#ffe50d 0,#ffe50d 100%,transparent 100%); }');
$('#fixes').append('article header.idea-bg-purple a { background-image: linear-gradient(0deg,transparent,transparent 50%,#88239a 0,#88239a 100%,transparent 100%); }');
$('#fixes').append('article header.idea-bg-blue a { background-image: linear-gradient(0deg,transparent,transparent 50%,#00a3f2 0,#00a3f2 100%,transparent 100%); }');
$('#fixes').append('article header.idea-bg-yellow a:focus, article header.idea-bg-yellow a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#ffe50d 0,#ffe50d 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff; color: #333;}');
$('#fixes').append('article header.idea-bg-pink a:focus, article header.idea-bg-pink a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#fe0096 0,#fe0096 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff; color: #333;}');
$('#fixes').append('article header.idea-bg-green a:focus, article header.idea-bg-green a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#00c493 0,#00c493 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff; color: #333;}');
$('#fixes').append('article header.idea-bg-purple a:focus, article header.idea-bg-purple a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#88239a 0,#88239a 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff; color: #333;}');
$('#fixes').append('article header.idea-bg-blue a:focus, article header.idea-bg-blue a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#00a3f2 0,#00a3f2 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff; color: #333;}');
$('#fixes').append('article header a:focus, article header a:hover {background-position: 0 -100%;}');
$('#fixes').append('article header a { background-repeat: no-repeat; background-position: 0 -185%; transition: all 0.3s cubic-bezier(.48,1.83,.0,.41); background-size: 100% 150%}');

// Case
// 
// 
$('.row.expanded:not(.bg-gray-light):not(.bg-pink) img').parent().css('margin-bottom', '40px');

// Contact Us
// 
// 
$('div[style*="baloons.jpg"]').next('div').addClass('bg-gray-light').addClass('contact-us-container').next('div').css('padding', '70px 0 0 0').children(':not(:last-child)').addClass('row').css('background', 'white');
$('div[style*="baloons.jpg"]').next('div').children(':last-child').css('background', 'white').css('margin-top', '70px');
$('#fixes').append('.contact-us-container .medium-5.columns {text-align: right;} .phrase { margin-top: -20px;} .contact-us-container>div:last-child(-1) { padding-bottom: 40px;}');

// Sök
// 
$('#fixes').append('@media screen and (min-width: 40em) .overlaysearch .query-wrapper .query-button .search-input-menu { font-size: 3rem;}');
$('#fixes').append('.input-search { color: #333;} .filter-on-text { display: none;}');

$('#fixes').append('.search-hit:nth-child(5n+1) h1 a { background-image: linear-gradient(0deg,transparent,transparent 50%,#fe0096 0,#fe0096 100%,transparent 100%); }');
$('#fixes').append('.search-hit:nth-child(5n+4) h1 a { background-image: linear-gradient(0deg,transparent,transparent 50%,#00c493 0,#00c493 100%,transparent 100%); }');
$('#fixes').append('.search-hit:nth-child(5n+3) h1 a { background-image: linear-gradient(0deg,transparent,transparent 50%,#ffe50d 0,#ffe50d 100%,transparent 100%); }');
$('#fixes').append('.search-hit:nth-child(5n+5) h1 a { background-image: linear-gradient(0deg,transparent,transparent 50%,#88239a 0,#88239a 100%,transparent 100%); }');
$('#fixes').append('.search-hit:nth-child(5n+2) h1 a  { background-image: linear-gradient(0deg,transparent,transparent 50%,#00a3f2 0,#00a3f2 100%,transparent 100%); }');
$('#fixes').append('.search-hit:nth-child(5n+3) h1 a:focus, .search-hit:nth-child(5n+3) h1 a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#ffe50d 0,#ffe50d 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff; color: #333;}');
$('#fixes').append('.search-hit:nth-child(5n+1) h1 a:focus, .search-hit:nth-child(5n+1) h1 a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#fe0096 0,#fe0096 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff; color: #333;}');
$('#fixes').append('.search-hit:nth-child(5n+4) h1 a:focus, .search-hit:nth-child(5n+4) h1 a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#00c493 0,#00c493 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff; color: #333;}');
$('#fixes').append('.search-hit:nth-child(5n+5) h1 a:focus, .search-hit:nth-child(5n+5) h1 a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#88239a 0,#88239a 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff; color: #333;}');
$('#fixes').append('.search-hit:nth-child(5n+2) h1 a:focus, .search-hit:nth-child(5n+2) h1 a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#00a3f2 0,#00a3f2 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff; color: #333;}');
$('#fixes').append('search-hit h1 a:focus, search-hit h1 a:hover {background-position: 0 -100%;}');
$('#fixes').append('search-hit h1 a { background-repeat: no-repeat; background-position: 0 -185%; transition: all 0.3s cubic-bezier(.48,1.83,.0,.41); background-size: 100% 150%}');
$('#fixes').append('.search-hits em { font-weight: 700; background-color: none; background: linear-gradient(to bottom, rgba(255,229,13,0) 0%,rgba(255,229,13,0) 85.9%,rgba(255,229,13,1) 86%,rgba(255,229,13,1) 100%);}');

// News
// 
// 
$('#news-container #result-container').addClass('padding-v-40');
$('#fixes').append('.news-common h1 { font-weight: 900;}');
$('#fixes').append('.news-common h1 p { line-height: 2.111rem;}');
$('#fixes').append('.news-common.link-bg-pink a { background-image: linear-gradient(0deg,transparent,transparent 9px,#fe0096 0,#fe0096 100%,transparent 100%); }');
$('#fixes').append('.news-common.link-bg-green a { background-image: linear-gradient(0deg,transparent,transparent 9px,#00c493 0,#00c493 100%,transparent 100%); }');
$('#fixes').append('.news-common.link-bg-yellow a { background-image: linear-gradient(0deg,transparent,transparent 9px,#ffe50d 0,#ffe50d 100%,transparent 100%); }');
$('#fixes').append('.news-common.link-bg-purple a { background-image: linear-gradient(0deg,transparent,transparent 9px,#88239a 0,#88239a 100%,transparent 100%); }');
$('#fixes').append('.news-common.link-bg-blue a { background-image: linear-gradient(0deg,transparent,transparent 9px,#00a3f2 0,#00a3f2 100%,transparent 100%); }');
$('#fixes').append('.news-common.link-bg-yellow a:focus, .news-common.link-bg-yellow a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#ffe50d 0,#ffe50d 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff}');
$('#fixes').append('.news-common.link-bg-pink a:focus, .news-common.link-bg-pink a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#fe0096 0,#fe0096 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff}');
$('#fixes').append('.news-common.link-bg-green a:focus, .news-common.link-bg-green a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#00c493 0,#00c493 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff}');
$('#fixes').append('.news-common.link-bg-purple a:focus, .news-common.link-bg-purple a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#88239a 0,#88239a 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff}');
$('#fixes').append('.news-common.link-bg-blue a:focus, .news-common.link-bg-blue a:hover {background-image: linear-gradient(0deg,transparent,transparent 50%,#00a3f2 0,#00a3f2 100%,transparent 100%); text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff}');
$('#fixes').append('.news-common a:focus, .news-common a:hover {background-position: 0 -100%;}');
$('#fixes').append('.news-common a { background-repeat: no-repeat; background-position: 0 -190%; transition: all 0.3s cubic-bezier(.48,1.83,.0,.41); background-size: 100% 150%}');
$('#fixes').append('article { padding: 40px;}');
$('#fixes').append('.news-common p{\
  position: relative;\
  max-height: 10.6rem; \
  overflow: hidden;\
  margin-bottom: 1rem;\
}\
\
.news-common p:after {\
  content: "";\
  text-align: right;\
  position: absolute;\
  bottom: 0;\
  right: 0;\
  width: 50%;\
  height: 1.2em;\
  background: linear-gradient(to right, rgba(255, 255, 255, 0), rgba(255, 255, 255, 1) 80%);\
}\
\
/* Now add in code for the browsers that support -webkit-line-clamp and overwrite the non-supportive stuff */\
@supports (-webkit-line-clamp: 6) {\
  .news-common p {\
      overflow: hidden;\
      text-overflow: ellipsis;\
      display: -webkit-box;\
      -webkit-line-clamp: 6;\
      -webkit-box-orient: vertical;\
  }\
  \
  .news-common p:after {\
    display: none;\
  }\
}');
$('#fixes').append('.vcard .photo {width: 140px; border: 8px solid white;}');


// Buttons
// 
// 
$('#fixes').append('.Form__MainBody .FormSubmitButton { font-family: Roboto, sans-serif; font-weight: 900;}');
$('.footer-button a.font-reeniebeanie').removeClass('font-size-2').css('font-size','2.5em');
$('#fixes').append('.button { font-size: 21px;}');
$('#fixes').append('a[href$=".doc"]:before, a[href$=".docx"]:before, a[href$=".pdf"]:before, a[href$=".ppt"]:before, a[href$=".pptx"]:before { position: relative; top: 25px;}');

// Vcard
// 
// 
$('#fixes').append('.vcard-info .title, .vcard-info .org { font-weight: 400; line-height: 20px;} .vcard-info .org {margin-bottom: 20px;} .vcard-info .adr, .vcard-info .tel, .vcard-info .email  {line-height: 20px;}');

// Footer
// 
// 
$('#fixes').append('.footer-projects h1 { font-size: 1.667rem;}');
$('#fixes').append('.footer-links { font-size: 1.167rem;}');
$('#fixes').append('.footer-links a { transition: all 0.3s cubic-bezier(.48,1.83,.0,.41); border-bottom: none; background-repeat: no-repeat; background-size: 100% 150%; background-position: 0 11px; background-image: linear-gradient(0deg,transparent,transparent 12px,#fe0096 0,#fe0096 24px,transparent 24px); text-shadow: -1px -1px 0 #333, 1px -1px 0 #333, -1px 1px 0 #333, 1px 1px 0 #333}');
$('#fixes').append('.footer-links a:focus, .footer-links a:hover {background-image: linear-gradient(0deg,transparent,transparent 12px,#fe0096 0,#fe0096 24px,transparent 24px); text-shadow: -1px -1px 0 #333, 1px -1px 0 #333, -1px 1px 0 #333, 1px 1px 0 #333; background-position: 0 2px;}');


// Investor Relations
//

$('#fixes').append('.sub-menu .licontent { width: auto; vertical-align: baseline; display: inline} .sub-menu .button {font-size: inherit;}');

