# HiQ.se Designer's Cut #

In short, this is a playground file where Ace Creative try tweaking some settings that hopefully will make it into the site as future changes.
At the moment the main focus of the script is to see how close to the design files we can get the site with some javascript trickery. 

## Current Status & Roadmap ##

### Current State ###
* Currently initial fixes that cover most of the site in the large mode are done, this does not include IR pages.

### Roadmap ###
* Testing Large fixes on other platforms than chrome
* Medium viewport fixes
* Retina images
* Better interactions and animations
* Remove page "jumping" due to overlay (Needs DOM restructure)
* IR pages (Needs DOM restructure)
* New cookie message
* (?)Small viewport fixes (Are userscripts possible on mobile?)

## Key info ##

* Visual improvements to HiQ.se
* Not meant for production, just some playing around.
* Current release: 0.1.x
* So far only tested on current macOS Chrome running Violentmonkey

## How do I get set up? ##

1. Make sure you have user scripts enabled in your browser (these instructions refer to the latest versions of the browser):

	* Firefox - install [Violent Monkey](https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/) or [Greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/).
	* Chrome - install [Violent Monkey](https://chrome.google.com/webstore/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag?hl=en) or [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=chrome).
	* Opera - install [Violent Monkey](https://addons.opera.com/en/extensions/details/violent-monkey/) or [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=opera).
	* Safari - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=safari).
	* Dolphin - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=dolphin).
	* UC Browser - install [Tampermonkey](https://tampermonkey.net/?ext=dhdg&browser=ucweb).
	
2. Once you're done with step 1, install [HiQ Designer's Cut](https://bitbucket.org/nkpgSaberRiders/hiq.se-designers-cut/raw/master/hiqdc.user.js)

3. Enjoy.

## Updating ##

Userscripts are set up to automatically update. You can check for updates from within the Greasemonkey or Tampermonkey menu, or click on the install link again to get the update.

## Issues

Please report any userscript issues within this repository's [issue section](https://bitbucket.org/nkpgSaberRiders/hiq.se-designers-cut/issues?status=new&status=open).

## Who do I talk to? ##

* This repo is managed by David Stenbeck (david.stenbeck@hiq.se for questions)